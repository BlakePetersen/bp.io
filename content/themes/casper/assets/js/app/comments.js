define(function() {

  // Declare Public Functions
  var initialize;

  // Define Public Functions
  initialize = function(postURL, postID) {
    window.disqus_config = function () {
      this.page.url = postURL;  // Replace PAGE_URL with your page's canonical URL variable
      this.page.identifier = postID; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
    };

    (function() {  // DON'T EDIT BELOW THIS LINE
      var d = document, s = d.createElement('script');
      s.src = '//blakepetersen.disqus.com/embed.js';
      s.setAttribute('data-timestamp', +new Date());
      (d.head || d.body).appendChild(s);
    })();
  };

  // Return Public Functions
  return {
    initialize: initialize
  }

});