define([
  'jquery',
  'lodash',
  'trianglify'
], function($, _, Trianglify) {

  var init = function () {
    $(window).on('load resize', _.throttle(function(){
      _generatePattern('header.no-cover .trianglify', 't1');
      _generatePattern('aside .no-cover', 't2');
    }, 50));
  };

  var _generatePattern = function(target, type) {

    var $target = $(target),
        options = {
          noiseIntensity: 0,
          x_gradient: colorbrewer.PuBuGn[9],
          y_gradient: colorbrewer.Greys[9],
          cellsize: $target.height() * .2
        };

    /* -- Secondary Pattern -- */
    if ( type === 't2' ) {
      options.x_gradient = ["#888888","#cdcdcd","#e9e9e9","#ffffff","#e9e9e9","#cdcdcd","#888888"];
      options.y_gradient = ["#888888","#cdcdcd","#e9e9e9","#ffffff","#e9e9e9","#cdcdcd","#888888"];
      options.cellsize = $target.height() * .1;
    }

    var pattern = new Trianglify(options),
        patternSVG = pattern.generate($target.width(), $target.height());

    $target.css('background-image', patternSVG.dataUrl).addClass('activate');
  };

  return {
    init: init
  }

});
