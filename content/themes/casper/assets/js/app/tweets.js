define([
  'jquery',
  'twitter'
], function($, twitterFetcher) {

  var $tweetContainer = $('.tweet-container');

  var init = function() {
    var mainConf = {
        "id": '668665593759821824',
        "domId": '',
        "maxTweets": 1,
        "enableLinks": true,
        "showPermalinks": false,
        "showInteraction": false,
        "showUser": false,
        "customCallback": _handleTweets
    };
    $tweetContainer.addClass('activate');
    twitterFetcher.fetch(mainConf);
  };

  var _handleTweets = function (tweets) {
    var x = tweets.length,
        n = 0,
        element = document.getElementById('tweet'),
        // Todo(blake): clean this up yo
        // html = new document.createDocumentFragment();
        html = '<ul>';

    while (n < x) {
      html += '<li>' + tweets[n] + '</li>';
      n++;
    }

    html += '</ul>';
    element.innerHTML = html;
    $tweetContainer.removeClass('stashed');
  };

  return {
    init: init,
    $tweetContainer: $tweetContainer
  }

});
