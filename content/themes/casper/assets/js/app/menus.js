define([
  'jquery',
  'lodash'
], function($, _){

  var init = function (){
    $('header.no-cover, aside .no-cover').addClass('activate');

    $(".menu-button, .nav-cover, .nav-close").on("click", _.throttle(function(e){
      e.preventDefault();
      $("body").toggleClass("nav-opened nav-closed");
    }, 50));
  };

  return {
    init: init
  }

});