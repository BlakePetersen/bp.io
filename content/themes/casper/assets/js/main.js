requirejs.config({
  baseUrl: '/assets/js',

  paths: {
    // Libs
    d3: 'lib/d3.min',
    jquery: 'lib/jquery-1.11.3.min',
    lodash: 'lib/lodash-2.0.0',
    prism: 'lib/prism.min',
    trianglify: 'lib/trianglify.min',
    twitter: 'lib/twitter',

    // App
    comments: 'app/comments',
    menus: 'app/menus',
    triangles: 'app/triangles',
    tweets: 'app/tweets'
  },

  shim: {
    //ghost: {
    //  deps: ['jquery', 'trianglify', 'twitter']
    //},
    trianglify: {
      deps: ['d3'],
      exports: 'Trianglify'
    },
    //twitter: {
    //  exports: 'twitterFetcher'
    //}
  }
});

requirejs([
  'menus',
  'triangles',
  'tweets'
], function (Menus, Triangles, Tweets) {

  Menus.init();
  Triangles.init();
  Tweets.init();

});
